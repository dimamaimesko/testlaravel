<?php

namespace App\Http\Controllers;
use App\Models\Students;
use Illuminate\Support\Facades\DB;

class ExportController extends Controller
{
    public function __construct()
    {

    }

    public function welcome()
    {
        return view('hello');
    }

    /**
     * View all students found in the database
     */
    public function viewStudents()
    {
        $students = Students::with('course')->get();

        return view('view_students', compact(['students']));
    }

    public function convert()
    {

        $chosen = [];
        $students = Students::all();
        foreach ($students as $student) {
            $data = $student->id;
            if ($data == request($student->id)){
                $chosen[] = $student->id;
            }

        }
       // print_r($chosen);
        $this->exportStudentsToCSV($chosen);
        $this->exportCourseAttendenceToCSV($chosen);
    }

    /**
     * Exports all student data to a CSV file
     */
    public function exportStudentsToCSV($chosen = [])
    {
        $students = Students::with('course')->whereIn('id', $chosen)->get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->build($students, ['id','firstname','surname', 'email', 'nationality','address_id','course_id','created_at','updated_at'])->download();

    }

    /**
     * Exports the total amount of students that are taking each course to a CSV file
     */
    public function exportCourseAttendenceToCSV($chosen = [])
    {

        $students =\App\Models\Course::select('course.course_name AS Course',DB::raw('count(course_name) AS Attendance'))
                                       ->join('student','student.course_id', '=', 'course.id')
                                       ->groupBy('course_name')
                                       ->whereIn('student.id', $chosen)
                                       ->get();

        $csvExporter = new \Laracsv\Export();
        $csvExporter->build($students, ['Course','Attendance'])->download();

    }

}

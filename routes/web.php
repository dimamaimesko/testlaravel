<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('', [ 'uses' => 'ExportController@welcome', 'as' => 'home'] );
Route::get('view', [ 'uses' => 'ExportController@viewStudents', 'as' => 'view'] );
//Route::get('export', [ 'uses' => 'ExportController@export', 'as' => 'export'] );


Route::get('/exportStudentsToCSV', 'ExportController@exportStudentsToCSV');
Route::get('/exportAttendenceToCSV', 'ExportController@exportCourseAttendenceToCSV');

Route::get('/convert', 'ExportController@convert');